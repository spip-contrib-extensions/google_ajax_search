<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'googleajaxsearch_description' => 'Anciennement appelé google ajaxsearch, Custom search permet de placer un widget de formulaire de recherche google.',
	'googleajaxsearch_nom' => 'Google Custom Search',
	'googleajaxsearch_slogan' => 'Formulaire de recherche google',
);

?>